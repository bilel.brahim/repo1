<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Exemple d'une application web avec maven</title>
</head>
<body>

	<h1>Exemple d'une application web avec maven</h1>

	<s:form action="addBookAction">
		<s:textfield name="name" label="Nom" value="" />
		<s:textfield name="authorName" label="Nom de l'auteur" value="" />
		<s:textfield name="price" label="Price" value="" />
	</s:form>

	<s:if test="bookList !null && bookList.size() > 0">
		<h2>Livres disponibles</h2>
		<table border="1px" cellpadding="8px">
			<tr>
				<th>ID livre</th>
				<th>Nom</th>
				<th>Nom de l'auteur</th>
				<th>Prix</th>
				<th>Date de sortie</th>
			</tr>
			<s:iterator value="bookList" status="userStatus">
				<tr>
					<td><s:property value="bookId"/></td>
					<td><s:property value="name"/></td>
					<td><s:property value="authorName"/></td>
					<td><s:property value="price"/></td>
					<td><s:date name="issuedDate" format="dd/MM/yyyy"/></td>
				</tr>
			</s:iterator>
		</table>
	</s:if>
</body>
</html>