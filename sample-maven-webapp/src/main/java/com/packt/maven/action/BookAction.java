package com.packt.maven.action;

import java.util.ArrayList;
import java.util.List;

import com.opensymphony.xwork2.ModelDriven;
import com.packt.maven.model.Book;

public class BookAction implements ModelDriven<Book>{

	Book book = new Book();
	List<Book> bookList = new ArrayList<Book>();
	public Book getModel() {
		return book;
	}
	
	public List<Book> getBookList(){
		return bookList;
	}
	
	public void setBookList(List<Book> bookList){
		this.bookList = bookList;
	}
	
	public String addBook() throws Exception{
		return "success";
	}
	
	public String listBook() throws Exception{
		return "success";
	}

}
